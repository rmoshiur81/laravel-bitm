@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        {{ $gallery->name }}
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <tr>
                                    <td>Name</td>
                                    <td>Description</td>
                                    <td>Created</td>
                                    <td>Updated</td>
                                </tr>
                                <tr>
                                    <td>{{ $gallery->name }}</td>
                                    <td>{{ $gallery->description }}</td>
                                    <td>{{ $gallery->created_at->toDayDateTimeString() }}</td>
                                    <td>{{ $gallery->updated_at->toDayDateTimeString() }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>


                    @foreach($videos as $video)

                        <div class="embed-responsive-item embed-responsive-16by9">
                            @if($video->provider=='Y')
                                {{--<object width="420"--}}
                                {{--data="{{ $video->source }}">--}}
                                {{--</object>--}}
                                <iframe class="embed-responsive-item" src="{{ $video->source }} "
                                        allowfullscreen></iframe>
                            @else
                                <div class="fb-video"
                                     data-href="{{ $video->source }}"
                                     data-width="300" data-show-text="false">
                                </div>

                            @endif
                        </div>

                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection
