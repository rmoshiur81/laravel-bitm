<div class="form-group">
    {!! Form::label('name', 'Gallery Name') !!}
    {!! Form::text('name', isset($gallery->name) ? $gallery->name : null, ['class'=> 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('name', 'Gallery Name') !!}
    {!! Form::textarea('description',  isset($gallery->description) ? $gallery->description : null, ['class'=> 'form-control']) !!}
</div>
@if(isset($gallery))
    <div class="form-group">
        <div class="radio-inline">
            {!! Form::label('display', 'Display?') !!}
        </div>

        @if($gallery->display=='Y')
            {!! Form::radio('display', 'Y', true) !!} Yes
            {!! Form::radio('display', 'N') !!} No
        @else
            {!! Form::radio('display', 'Y') !!} Yes
            {!! Form::radio('display', 'N', true) !!} No
        @endif

    </div>
@else
    <div class="form-group">
        <div class="radio-inline">
            {!! Form::label('display', 'Display?') !!}
        </div>
        {!! Form::radio('display', 'Y', true) !!} Yes
        {!! Form::radio('display', 'N') !!} No
    </div>
@endif

<div class="form-group">
    {!! Form::label('galleries', 'Select Video') !!}
    {{--{{ dd($galleries) }}--}}
    @if(isset($videos))
        @foreach($videos as $id => $video)

            <tr>
                <div class="panel-heading">
                    {!! Form::checkbox('video_ids[]', $video->id ) !!} {{ $video->title}}
                    <td>{{ $video->title }}</td>
                </div>

                <td>
                    <div class="embed-responsive-item embed-responsive-16by9">
                        @if($video->provider=='Y')
                            {{--<object width="420"--}}
                            {{--data="{{ $video->source }}">--}}
                            {{--</object>--}}
                            <iframe class="embed-responsive-item" src="{{ $video->source }} "
                                    allowfullscreen></iframe>
                        @else
                            <div class="fb-video"
                                 data-href="{{ $video->source }}"
                                 data-width="300" data-show-text="false">
                            </div>

                        @endif
                    </div>
                </td>
            </tr>
        @endforeach
    @endif
</div>


@if(isset($gallery->id))
    {!! Form::hidden('id', $gallery->id) !!}
@endif
<div class="form-group">
    {!! Form::submit('Submit', null, ['class'=> 'form-control']) !!}
</div>
