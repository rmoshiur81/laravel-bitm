@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    @if(Session::has('message'))
                        {{ Session::get('message') }}
                    @endif

                    {{--{{ $galleries }}--}}
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <th>ID</th>
                            <th>Name</th>
                            <th> Action</th>
                            @foreach($galleries as $gallery)
                                <tr>
                                    <td>{{ $gallery->id }}</td>
                                    <td>{{ $gallery->name }}</td>
                                    <td>
                                        <a href="{{ url('/galleries/'. $gallery->id) }}">View</a>
                                        <a href="{{ url('/galleries/'. $gallery->id. '/edit') }}">Edit</a>

                                        {{ Form::open([
                                        'method'=> 'DELETE',
                                        'url' => ['/galleries', $gallery->id],
                                        ]) }}

                                        {{ Form::submit('Delete', ['class' => 'btn']) }}

                                        {{ Form::close() }}


                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
