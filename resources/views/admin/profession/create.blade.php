@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">



                    <div class="panel-body">
                        {!! Form::open(['url' => '/professions']) !!}

                        <div class="form-group">
                            {!! Form::label('Name') !!}
                            {!! Form::text('name', null, ['class'=> 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Submit', null, ['class'=> 'form-control']) !!}
                        </div>


                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
