@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    @if(Session::has('message'))
                        {{ Session::get('message') }}
                    @endif
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <th>ID</th>
                            <th>Name</th>
                            <th> Action</th>
                            @foreach($professions as $profession)
                                <tr>
                                    <td>{{ $profession->id }}</td>
                                    <td>{{ $profession->name }}</td>
                                    <td>
                                        <a href="{{ url('/professions/'. $profession->id) }}">View</a>
                                        <a href="{{ url('/professions/'. $profession->id. '/edit') }}">Edit</a>

                                       {{ Form::open([
                                       'method'=> 'DELETE',
                                       'url' => ['/professions', $profession->id],
                                       ]) }}

                                        {{ Form::submit('Delete', ['class' => 'btn']) }}

                                        {{ Form::close() }}


                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
