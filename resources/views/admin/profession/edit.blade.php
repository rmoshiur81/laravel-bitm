@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">



                    <div class="panel-body">
                        {!! Form::open(['method'=>'PATCH', 'url' => '/professions/'. $professoin->id]) !!}

                        <div class="form-group">
                            {!! Form::label('Name') !!}
                            {!! Form::text('name', isset($professoin->name) ? $professoin->name : null, ['class'=> 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Update', null, ['class'=> 'form-control']) !!}
                        </div>


                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
