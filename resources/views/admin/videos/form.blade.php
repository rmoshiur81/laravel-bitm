<div class="embed-responsive-item embed-responsive-16by9">
    @if(isset($video))
        @if($video->provider=='Y')
            {{--<object width="420"--}}
            {{--data="{{ $video->source }}">--}}
            {{--</object>--}}
            <iframe class="embed-responsive-item" src="{{ $video->source }} "
                    allowfullscreen></iframe>
        @else
            <div class="fb-video"
                 data-href="{{ $video->source }}"
                 data-width="300" data-show-text="false">
            </div>

        @endif
    @endif
</div>


<div class="form-group">
    {!! Form::label('title', 'Video Title') !!}
    {!! Form::text('title', isset($video->title) ? $video->title : null, ['class'=> 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('summary', 'Video Summary') !!}
    {!! Form::textarea('summary',  isset($video->summary) ? $video->summary : null, ['class'=> 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('source', 'Video URL') !!}
    {!! Form::url('source',  isset($video->source) ? $video->source : null, ['class'=> 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('provider', 'Video Provider') !!}
    @if(isset($video->provider) && ($video->provider=='F'))
        {!! Form::radio('provider', 'F', true) !!} Facebook
        {!! Form::radio('provider', 'Y') !!} Youtube
    @elseif(isset($video->provider) && ($video->provider=='Y'))
        {!! Form::radio('provider', 'F') !!} Facebook
        {!! Form::radio('provider', 'Y', true) !!} Youtube
    @else
        {!! Form::radio('provider', 'F') !!} Facebook
        {!! Form::radio('provider', 'Y', true) !!} Youtube
    @endif
</div>

<div class="form-group">
    {!! Form::label('galleries', 'Select Gallery') !!}
    {{--{{ dd($galleries) }}--}}
    {{--{{ dd($selected_galleries) }}]--}}
    @foreach($galleries as  $gallery)
        {!! Form::checkbox('galleries_ids[]', $gallery->id, in_array($gallery->id, $selected_galleries) ) !!} {{ $gallery->name }}
    @endforeach
</div>


@if(isset($gallery))
    <div class="form-group">
        <div class="radio-inline">
            {!! Form::label('display', 'Display?') !!}
        </div>

        @if($gallery->display=='Y')
            {!! Form::radio('display', 'Y', true) !!} Yes
            {!! Form::radio('display', 'N') !!} No
        @else
            {!! Form::radio('display', 'Y') !!} Yes
            {!! Form::radio('display', 'N', true) !!} No
        @endif

    </div>
@else
    <div class="form-group">
        <div class="radio-inline">
            {!! Form::label('display', 'Display?') !!}
        </div>
        {!! Form::radio('display', 'Y', true) !!} Yes
        {!! Form::radio('display', 'N') !!} No
    </div>
@endif


@if(isset($gallery->id))
    {!! Form::hidden('id', $gallery->id) !!}
@endif
<div class="form-group">
    {!! Form::submit('Submit', null, ['class'=> 'form-control']) !!}
</div>
