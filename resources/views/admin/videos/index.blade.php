@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    @if(Session::has('message'))
                        {{ Session::get('message') }}
                    @endif

                    {{--{{ $galleries }}--}}
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <th>ID</th>
                            <th>Title</th>
                            <th>Video</th>
                            <th>Category</th>
                            <th> Action</th>
                            @foreach($videos as $video)
                                <tr>
                                    <td>{{ $video->id }}</td>
                                    <td>{{ $video->title }}</td>
                                    <td>
                                        <div class="embed-responsive-item embed-responsive-16by9">
                                            @if($video->provider=='Y')
                                                {{--<object width="420"--}}
                                                {{--data="{{ $video->source }}">--}}
                                                {{--</object>--}}
                                                <iframe class="embed-responsive-item" src="{{ $video->source }} "
                                                        allowfullscreen></iframe>
                                            @else
                                                <div class="fb-video"
                                                     data-href="{{ $video->source }}"
                                                     data-width="300" data-show-text="false">
                                                </div>

                                            @endif
                                        </div>
                                    </td>
                                    <td>


                                        @foreach( $video->galleries as $gallery)

                                            <a href=" {{ url('/galleries', $gallery->id) }}"
                                               class="label label-primary"> {{ $gallery->name }}</a>

                                        @endforeach


                                    </td>
                                    <td>
                                        <a href="{{ url('/galleries/'. $video->id) }}">View</a>
                                        <a href="{{ url('/videos/'. $video->id. '/edit') }}">Edit</a>

                                        {{ Form::open([
                                        'method'=> 'DELETE',
                                        'url' => ['/galleries', $video->id],
                                        ]) }}

                                        {{ Form::submit('Delete', ['class' => 'btn']) }}

                                        {{ Form::close() }}


                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
