@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">


                        <div class="panel-body">
                            {!! Form::open(['url' => '/user/store']) !!}

                            <div class="form-group">
                                {!! Form::label('Name') !!}
                                {!! Form::text('name', null, ['class'=> 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Email') !!}
                                {!! Form::email('email', null, ['class'=> 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Profession') !!}
                                {!! Form::select('profession_id', $professions, null, ['placeholder' => 'Select ...', 'class'=> 'form-control']) !!}
                            </div>


                            <div class="form-group">
                                {!! Form::label('Password') !!}
                                {!! Form::password('password', ['class'=> 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('bio') !!}
                                {!! Form::textarea('bio', null, ['class'=> 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Web') !!}
                                {!! Form::url('web', null, ['class'=> 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Facebook') !!}
                                {!! Form::url('facebook', null, ['class'=> 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Twitter') !!}
                                {!! Form::url('twitter', null, ['class'=> 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Github') !!}
                                {!! Form::url('github', null, ['class'=> 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Submit', null, ['class'=> 'form-control']) !!}
                            </div>


                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
