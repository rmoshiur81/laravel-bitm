@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">


                    <div class="panel-body">
                        {!! Form::open(['url' => '/user/'. $user->id. '/update']) !!}

                        <div class="form-group">
                            {!! Form::label('Name') !!}
                            {!! Form::text('name', $user->name, ['class'=> 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Profession') !!}
                            {!! Form::select('profession_id', $professoins, isset($user->profession_id) ? $user->profession_id : null ) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('bio') !!}
                            {!! Form::textarea('bio', $user->profile['bio'], ['class'=> 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Web') !!}
                            {!! Form::url('web', $user->profile['web'], ['class'=> 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Facebook') !!}
                            {!! Form::url('facebook', $user->profile['facebook'], ['class'=> 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Twitter') !!}
                            {!! Form::url('twitter', $user->profile['twitter'], ['class'=> 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Github') !!}
                            {!! Form::url('github', $user->profile['github'], ['class'=> 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Submit', null, ['class'=> 'form-control']) !!}
                        </div>


                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
