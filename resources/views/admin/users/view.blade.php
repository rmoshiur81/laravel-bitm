@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    @if(Session::has('message'))
                        {{ Session::get('message') }}
                    @endif
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Facebook</th>
                            <th>Twitter</th>
                            <th>Action</th>

                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <a href="{{ $user->profile['facebook'] }}" target="_blank">
                                        {{ $user->profile->facebook }}</a>
                                </td>
                                <td>{{ $user->profile['twitter'] }}</td>
                                <td>
                                    <a href="{{ url('/user/'. $user->id) }}">View</a>
                                    <a href="{{ url('/user/'. $user->id. '/edit') }}">Edit</a>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
