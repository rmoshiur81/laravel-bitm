<?php

namespace App\Http\Controllers;

use App\Profession;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::with('profile', 'profession')->get();
//        dd($users);
        return view('admin.users.index', ['allUsers' => $users]);
    }


    public function create()
    {
        $professions = Profession::pluck('name', 'id');
        return view('admin.users.create', ['professions' => $professions]);
    }


    public function store(Request $request)
    {

        $data = $request->only('name', 'email', 'profession_id');
        $data['password'] = password_hash($request->password, PASSWORD_BCRYPT);
        $user = User::create($data);

        $profileData = $request->only('bio', 'web', 'facebook', 'twitter', 'github');
        $user->profile()->create($profileData);

        Session::flash('message', 'User Successfully Added');
        return redirect('/users');
    }


    public function show($id)
    {
        $user = User::find($id);
        return view('admin.users.view', ['user' => $user, 'something' => 'ok']);
    }

    public function edit($id)
    {
        $user = User::find($id);
        $professoins = Profession::pluck('name', 'id');
        return view('admin.users.edit', ['user' => $user, 'professoins'=> $professoins]);
    }


    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $data = $request->only('name', 'profession_id');

        $user->update($data);

        $profileData = $request->only('bio', 'web', 'facebook', 'twitter', 'github');
        $user->profile()->update($profileData);

        Session::flash('message', 'User Successfully Updated');
        return redirect('/users');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $name = $user->name;
        echo "$name going to destory";
        $user->destroy($id);
        Session::flash('message', "$name Successfully Deleted");
        return redirect('/users');
    }

    public function abc()
    {
        return view('admin.users.abc');
    }
}
