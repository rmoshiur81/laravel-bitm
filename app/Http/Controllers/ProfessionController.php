<?php

namespace App\Http\Controllers;

use App\Profession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProfessionController extends Controller
{

    public function index()
    {
        $professoins = Profession::all();
        return view('admin.profession.index', ['professions' => $professoins]);
    }


    public function create()
    {
        return view('admin.profession.create');
    }


    public function store(Request $request)
    {
        $data = $request->only('name');
        $professoin = Profession::create($data);
        Session::flash('message', 'Profession Added');
        return redirect('/professions');
//        return redirect()->back();
    }


    public function show($id)
    {
        $profession = Profession::find($id);
        return view('admin.profession.view', ['profession' => $profession]);
    }


    public function edit($id)
    {
        $profession = Profession::findorfail($id);
        return view('admin.profession.edit', ['professoin' => $profession]);
    }


    public function update(Request $request, $id)
    {
        $professon = Profession::findorfail($id);
        $data = $request->only('name');
        $professon->update($data);
        Session::flash('message', 'Profession Updated');
        return redirect('/professions');
    }


    public function destroy($id)
    {
        $professoin = Profession::find($id);
        $name = $professoin->name;
        $professoin->delete();
        Session::flash('message', " $name Has been deleted");
        return redirect('/professions');
    }
}
