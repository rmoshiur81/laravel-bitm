<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Http\Requests\VideoRequest;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::with('galleries')->get();
        return view('admin.videos.index', ['videos' => $videos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $galleries = Gallery::pluck('name', 'id');
        $galleries = Gallery::all();
        $selected_galleries = [];
        return view('admin.videos.create', ['galleries' => $galleries, 'selected_galleries' => $selected_galleries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoRequest $request)
    {

        $data = $request->only(['title', 'source', 'display', 'provider', 'summary']);
        $video = Video::create($data);
        $gallery_ids = $request->input('galleries_ids');
        $video->galleries()->attach($gallery_ids);

        Session::flash('message', 'Video Added');
        return redirect('/videos');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video, Request $request)
    {
        $galleries = Gallery::all();
//        dd($galleries);
        $selected_gallereis = $video->galleries()->pluck('id')->toArray();
//        $selected_gallereis = [];
//        dd($selected_gallereis);
        return view('admin.videos.edit', ['galleries' => $galleries, 'selected_galleries' => $selected_gallereis, 'video' => $video]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Video $video, VideoRequest $request)
    {
        $data = $request->only(['title', 'source', 'display', 'provider', 'summary']);
        $video->update($data);
        $gallery_ids = $request->input('galleries_ids');
        $video->galleries()->sync($gallery_ids);

        Session::flash('message', 'Video Updated');
        return redirect('/videos');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
