<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Http\Requests\GalleryRequest;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = Gallery::all();
        return view('admin.galleries.index', ['galleries' => $galleries]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $videos = Video::all();

        return view('admin.galleries.create', ['videos' => $videos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Gallery $gallery, GalleryRequest $request)
    {
        $data = $request->only('name', 'description', 'display');
        $gal = Gallery::create($data);
        $video_ids = $request->input('video_ids');

        $gal->videos()->attach($video_ids);

        Session::flash('message', 'Video Added');
        return redirect('/galleries');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        $videos = $gallery->videos()->get();

        return view('admin.galleries.show', ['gallery' => $gallery, 'videos' => $videos]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = Gallery::find($id);
//        dd($gallery);
        return view('admin.galleries.edit', ['gallery' => $gallery]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Gallery $gallery, GalleryRequest $request)
    {
        $data = $request->only('name', 'description', 'display');
        $gallery->update($data);
        Session::flash('message', 'Gallery updated');

        return redirect('/galleries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
