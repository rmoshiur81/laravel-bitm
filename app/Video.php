<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = ['title', 'summary', 'source', 'provider', 'display'];

    public function galleries()
    {
        return $this->belongsToMany('App\Gallery');
    }
}
