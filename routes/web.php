<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/users', 'UserController@index');
Route::get('/abc', 'UserController@abc');
Route::get('/user/add', 'UserController@create');
Route::post('/user/store', 'UserController@store');
Route::get('/user/{id}', 'UserController@show');
Route::get('/user/{id}/edit', 'UserController@edit');
Route::post('/user/{id}/update', 'UserController@update');
Route::get('/user/{id}/delete', 'UserController@destroy');

Route::resource('professions', 'ProfessionController');
Route::resource('galleries', 'GalleryController');
Route::resource('videos', 'VideoController');